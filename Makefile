CC=gcc
C_FLAGS=-ansi -pedantic -Wall

C_FILES=utils/errors.c utils/strings.c utils/conversions.c \
interpreter/interpreter.c fitness/fitness.c population/population.c \
evolution/evolution.c generator/generator.c main.c

EXECUTABLE=tm_generator

all: build

build:
	$(CC) $(C_FLAGS) $(C_FILES) -o $(EXECUTABLE)

clean:
	rm -rf $(EXECUTABLE)
