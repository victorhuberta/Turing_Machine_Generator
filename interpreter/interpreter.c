/**
 * Module: interpreter
 * Author: Victor Huberta
 * ```
 * Act as every generated machine's interpreter. It interprets
 * a machine's specification. It is the one responsible for
 * testing the solutions provided by generator.
 */

#include "interpreter.h"

/**
 * Create a machine based on the given specification.
 */
Machine *create_machine(char *spec)
{
    char **states, **ops;
    Machine *machine;
    State *state;
    int s, op, n_states, n_ops;

    states = str_split(spec, ST_DELIM);
    for (n_states = 0; states[n_states] != 0; ++n_states);

    machine = new_machine(NULL, n_states);

    for (s = 0; s < n_states; ++s) {
        ops = str_split(states[s], OP_DELIM);
        for (n_ops = 0; ops[n_ops] != 0; ++n_ops);

        state = new_state(NULL, n_ops);

        for (op = 0; op < n_ops; ++op) {
            state->ops[op] = new_operation
                (ops[op][READ], ops[op][WRITE],
                    ops[op][SHIFT], char_to_int(ops[op][NEXT_ST]));
        }

        free_strings(ops);
        machine->states[s] = state;
    }

    free_strings(states);

    return machine;
}

/**
 * Create a new machine structure with empty properties
 * or fully loaded ones.
 */
Machine *new_machine(State **states, int n_states)
{
    Machine *machine = calloc(1, sizeof *machine);
    ptr_exit_on_err("calloc", machine);

    machine->states = (states != NULL) ?
        states : calloc(n_states, sizeof *(machine->states));
    ptr_exit_on_err("calloc", machine);
    machine->n_states = n_states;

    return machine;
}

/**
 * Create a new state structure with empty properties
 * or fully loaded ones.
 */
State *new_state(Operation **ops, int n_ops)
{
    State *state = calloc(1, sizeof *state);
    ptr_exit_on_err("calloc", state);

    state->ops = (ops != NULL) ? ops : calloc(n_ops, sizeof *(state->ops));
    ptr_exit_on_err("calloc", state->ops);
    state->n_ops = n_ops;

    return state;
}

/**
 * Create a new operation structure with empty properties
 * or fully loaded ones.
 */
Operation *new_operation(char r, char w, char shift, int next_state)
{
    Operation *operation = calloc(1, sizeof *operation);
    ptr_exit_on_err("calloc", operation);

    operation->r = r;
    operation->w = w;
    operation->shift = shift;
    operation->next_state = next_state;

    return operation;
}

/**
 * Run the created machine on the given input.
 * Halt it if any abnormal things happen or no more
 * input to be read. Return the output of the machine.
 */
char *run_machine(Machine *machine, char *input)
{
    int i = 0, h = 0, s = 0, op;
    char r, *output;
    Operation *operation;

    output = strdup(input);

    if (machine == NULL) return output;

    while ((r = output[h]) != '\0') {
        if (i++ >= N_ALLOWED_OPERATIONS) return output;

        for (op = 0;; ++op) {
            if (op >= machine->states[s]->n_ops) return output;

            operation = machine->states[s]->ops[op];
            if (operation->r == r) {
                output[h] = operation->w;

                h = (operation->shift == LEFT) ? h - 1 : h + 1;
                if (h < 0) return output;

                s = operation->next_state;
                if (s < 0 || s >= machine->n_states)
                    return output;
                break;
            }
        }
    }

    return output;
}

void free_machine(Machine *machine)
{
    int s, op;

    for (s = 0; s < machine->n_states; ++s) {
        for (op = 0; op < machine->states[s]->n_ops; ++op)
            free(machine->states[s]->ops[op]);
        free(machine->states[s]->ops);
        free(machine->states[s]);
    }

    free(machine->states);
    free(machine);
}

void test_interpreter(void)
{
    char *spec = "BBR1;BBR1,aaR1,bbR1,ccL2;abL2,baL2";
    char *input = "BabcabB";
    char *output; /* should be "BbacabB" */

    Machine *machine = create_machine(spec);
    output = run_machine(machine, input);
    printf("Output: %s\n", output);

    if (strcmp(output, "BbacabB") == 0)
        printf("OK: The interpreter works fine\n");

    free(output);
    free_machine(machine);
}
