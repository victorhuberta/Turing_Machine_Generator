/**
 * Module: evolution
 * Author: Victor Huberta
 * ```
 * Evolve the current population by randomly perform
 * crossover and mutation on chromosomes with relatively
 * high fitness.
 */

#include "evolution.h"

static char *alphabets;
static int n_alphabets;

void init_evolution(char *_alphabets, int _n_alphabets)
{
    alphabets = _alphabets;
    n_alphabets = _n_alphabets;
}

Chromosome **evolve(double total_fitness, Chromosome **population)
{
    Chromosome **new_population;

    new_population = get_new_population(total_fitness, population);

    free_population(population);
    return new_population;
}

/**
 * Randomly select two offsprings (still based on fitness), perform a
 * crossover on them, mutate them, and eventually add them into the new
 * population.
 */
Chromosome **get_new_population(double total_fitness, Chromosome **population)
{
    int p;
    Chromosome *offspring1, *offspring2;
    Chromosome **new_population = calloc(N_POPULATION, sizeof *new_population);
    ptr_exit_on_err("calloc", new_population);

    for (p = 0; p < N_POPULATION;) {
        offspring1 = natural_selection(total_fitness, population);
        offspring2 = natural_selection(total_fitness, population);

        if (offspring1 == NULL || offspring2 == NULL) continue;

        crossover(offspring1, offspring2);

        mutate(offspring1);
        mutate(offspring2);

        new_population[p++] = offspring1;
        new_population[p++] = offspring2;
    }

    return new_population;
}

/**
 * Randomly select chromosomes from the curent population. The randomness
 * is based on the fitness boundary. For every selected chromosome, copy
 * their stats and return the copy.
 */
Chromosome *natural_selection(double total_fitness, Chromosome **population)
{
    int p;
    double fitness_boundary = rand() % ((int) total_fitness + 1);
    double fitness_so_far = 0.0;

    for (p = 0; p < N_POPULATION; ++p) {
        fitness_so_far += population[p]->fitness;

        if (fitness_so_far >= fitness_boundary) {
            return new_chromosome
                (strdup(population[p]->spec),
                    population[p]->fitness,
                        population[p]->n_states);
        }
    }

    return NULL;
}

/**
 * Crossover is the process of replacing genes of a chromosome
 * with those of another. The chance of crossover is N_CROSSOVER_RATE.
 * We need to determine which of the chromosomes is shorter, because
 * in this context, every chromosome has different length.
 */
void crossover(Chromosome *offspring1, Chromosome *offspring2)
{
    Chromosome *shorter, *longer;
    double random = (double) rand() / (double) RAND_MAX;

    if (random < N_CROSSOVER_RATE) {
        shorter = (offspring1->n_states >= offspring2->n_states) ?
            offspring2 : offspring1;
        longer = (offspring1->n_states >= offspring2->n_states) ?
            offspring1 : offspring2;
        apply_crossover(shorter, longer);
    }
}

/**
 * Apply crossover by switching their genes. Reconstruct the
 * specification of the chromosome.
 */
void apply_crossover(Chromosome *shorter, Chromosome *longer)
{
    char **states1, **states2;

    states1 = str_split(shorter->spec, ST_DELIM);
    states2 = str_split(longer->spec, ST_DELIM);
    switch_genes(shorter, longer, states1, states2);

    free(shorter->spec);
    free(longer->spec);
    shorter->spec = get_spec_from_states(states1);
    longer->spec = get_spec_from_states(states2);

    free_strings(states1);
    free_strings(states2);
}

/**
 * Randomly take a gene from the longer chromosome, and switch
 * it with a gene from the shorter chromosome.
 */
void switch_genes(Chromosome *shorter, Chromosome *longer,
    char **states1, char **states2)
{
    int s, r, slice = rand() % shorter->n_states;
    char **tmp_states = str_split(shorter->spec, ST_DELIM);

    for (s = slice; s < shorter->n_states; ++s) {
        r = rand() % longer->n_states;

        free(states1[s]);
        states1[s] = strdup(states2[r]);

        free(states2[r]);
        states2[r] = strdup(tmp_states[s]);
    }

    free_strings(tmp_states);
}

/**
 * Reconstruct a chromosome's specification
 * from a list of states.
 */
char *get_spec_from_states(char **states)
{
    int s;
    char *spec = calloc(N_SPEC_LEN, sizeof *spec);
    ptr_exit_on_err("calloc", spec);

    for (s = 0; states[s] != 0; ++s) {
        strcat(spec, states[s]);

        if (states[s + 1] != 0)
            strcat(spec, ";");
    }

    return spec;
}

/**
 * Randomly mutate (modify) the genes of a chromosome.
 * Reconstruct the new specification of the chromosome.
 */
void mutate(Chromosome *chromosome)
{
    int s;
    char **states;
    char **ops;
    char *new_spec;

    states = str_split(chromosome->spec, ST_DELIM);

    for (s = 0; states[s] != 0; ++s) {
        ops = str_split(states[s], OP_DELIM);
        modify_operations(ops, chromosome);
        free(states[s]);
        states[s] = get_spec_from_ops(ops);
        free_strings(ops);
    }

    new_spec = get_spec_from_states(states);
    free_strings(states);

    free(chromosome->spec);
    chromosome->spec = new_spec;
}

/**
 * Modify a list of operations with a rate of N_MUTATION_RATE.
 */
void modify_operations(char **ops, Chromosome *chromosome)
{
    int c, op;
    double random;

    for (op = 0; ops[op] != 0; ++op) {
        for (c = 0; ops[op][c] != '\0'; ++c) {
            random = (double) rand() / (double) RAND_MAX;
            if (random < N_MUTATION_RATE)
                apply_mutation_on_op(ops[op], c, chromosome);
        }
    }
}

/**
 * Tweak a single operation by randomly replacing an alphabet
 * with another taken from the set of input alphabets.
 */
void apply_mutation_on_op(char *op, int c, Chromosome *chromosome)
{
    int r;

    if (c == READ || c == WRITE) {
        r = rand() % n_alphabets;
        op[c] = alphabets[r];
    } else if (c == SHIFT) {
        op[c] = (chromosome->spec[c] == LEFT) ? RIGHT : LEFT;
    } else if (c == NEXT_ST) {
        r = rand() % chromosome->n_states;
        op[c] = r + '0';
    }
}

/**
 * Reconstruct a smaller part of a chromosome's
 * specification from a list of operations.
 */
char *get_spec_from_ops(char **ops)
{
    int op;
    char *spec = calloc(N_SPEC_LEN, sizeof *spec);
    ptr_exit_on_err("calloc", spec);

    for (op = 0; ops[op] != 0; ++op) {
        strcat(spec, ops[op]);

        if (ops[op + 1] != 0)
            strcat(spec, ",");
    }

    return spec;
}
