#ifndef EVOLUTION_H
#define EVOLUTION_H

#include "../main.h"
#include "../population/population.h"

#define N_CROSSOVER_RATE 0.7
#define N_MUTATION_RATE 0.001

void init_evolution(char *_alphabets, int _n_alphabets);
Chromosome **evolve(double total_fitness, Chromosome **population);
Chromosome **get_new_population(double total_fitness, Chromosome **population);
Chromosome *natural_selection(double total_fitness, Chromosome **population);
void crossover(Chromosome *offspring1, Chromosome *offspring2);
void apply_crossover(Chromosome *smaller, Chromosome *bigger);
void switch_genes(Chromosome *smaller, Chromosome *bigger,
    char **states1, char **states2);
char *get_spec_from_states(char **states);
void mutate(Chromosome *chromosome);
void modify_operations(char **ops, Chromosome *chromosome);
void apply_mutation_on_op(char *op, int c, Chromosome *chromosome);
char *get_spec_from_ops(char **ops);

#endif
