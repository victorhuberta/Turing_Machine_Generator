/**
 * Module: debug
 * Author: Victor Huberta
 * ```
 * Provide utility functions for debugging purposes.
 */

#include "debug.h"

void print_operation(Operation *operation)
{
    printf("operation->r = %c\n", operation->r);
    printf("operation->w = %c\n", operation->w);
    printf("operation->shift = %c\n", operation->shift);
    printf("operation->next_state = %d\n", operation->next_state);
}
