#ifndef FITNESS_H
#define FITNESS_H

#include "../main.h"
#include "../interpreter/interpreter.h"
#include "../population/population.h"

double get_fitness(Chromosome *chromosome, char *input, char *expected);
int levenshtein(char *s1, char *s2);

#endif
