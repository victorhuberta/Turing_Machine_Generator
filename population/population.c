/**
 * Module: population
 * Author: Victor Huberta
 * ```
 * Responsible for gathering a new population from all
 * possible genes, and handling the population.
 */

#include "population.h"

static char *alphabets;
static int n_alphabets = 0;

void init_population(char *_alphabets, int _n_alphabets)
{
    alphabets = _alphabets;
    n_alphabets = _n_alphabets;
}

/**
 * Get a random population (a huge list of chromosomes)
 * from all possible genes (categorized by gene types).
 */
Chromosome **get_random_population(void)
{
    GeneType **gene_types;
    Chromosome **population;
    Chromosome *chromosome;
    int p, n_states = 0;

    population = calloc(N_POPULATION, sizeof *population);
    ptr_exit_on_err("calloc", population);

    srand((unsigned) time(NULL));

    for (p = 0; p < N_POPULATION; ++p) {
        n_states = (rand() % N_MAX_STATES) + 1;

        gene_types = get_all_genes(n_states);
        chromosome = new_chromosome(NULL, 0.0, n_states);
        write_spec_random(gene_types, chromosome, n_states);

        population[p] = chromosome;

        free_gene_types(gene_types);
    }

    return population;
}

/**
 * Create a new chromosome structure with empty properties
 * or fully loaded ones.
 */
Chromosome *new_chromosome(char *spec, double fitness, int n_states)
{
    Chromosome *chromosome = calloc(1, sizeof *chromosome);
    ptr_exit_on_err("calloc", chromosome);

    chromosome->spec = (spec != NULL) ?
        spec : calloc(N_SPEC_LEN, sizeof *(chromosome->spec));
    ptr_exit_on_err("calloc", chromosome->spec);

    chromosome->fitness = fitness;
    chromosome->n_states = n_states;

    return chromosome;
}

/**
 * Randomly write a new specification (genes) for
 * a chromosome by choosing genes from a set of all
 * possible genes.
 */
Chromosome *write_spec_random(GeneType **gene_types,
    Chromosome *chromosome, int n_states)
{
    int s, a, g;

    for (s = 0; s < n_states; ++s) {
        for (a = 0; a < n_alphabets; ++a) {
            g = rand() % gene_types[a]->n_genes;
            strcat(chromosome->spec, gene_types[a]->genes[g]);

            if (a < n_alphabets - 1)
                strcat(chromosome->spec, ",");
        }

        if (s < n_states - 1)
            strcat(chromosome->spec, ";");
    }

    return chromosome;
}

/**
 * Get all possible genes based on the given number of
 * states and the set of input alphabets. It is categorized
 * by gene types. For a set of input alphabets = {a, b, c, B},
 * the gene types are 'a', 'b', 'c', and 'B'.
 */
GeneType **get_all_genes(int n_states)
{
    GeneType **gene_types;
    GeneType *gene_type;
    int a, n_genes;
    char alphabet;

    gene_types = calloc(n_alphabets, sizeof *gene_types);
    ptr_exit_on_err("calloc", gene_types);

    n_genes = n_alphabets * N_SHIFT_TYPES * n_states;

    for (a = 0; a < n_alphabets; ++a) {
        alphabet = alphabets[a];

        gene_type = new_genetype
            (alphabet, new_genes(alphabet, n_genes, n_states), n_genes);

        gene_types[a] = gene_type;
    }

    return gene_types;
}

/**
 * Create a new gene type structure with empty properties
 * or fully loaded ones.
 */
GeneType *new_genetype(char alphabet, char **genes, int n_genes)
{
    GeneType *gene_type = calloc(1, sizeof *gene_type);
    ptr_exit_on_err("calloc", gene_type);

    gene_type->name = alphabet;
    gene_type->genes = (genes != NULL) ?
        genes : calloc(n_genes, sizeof *(gene_type->genes));
    ptr_exit_on_err("calloc", gene_type->genes);

    gene_type->n_genes = n_genes;

    return gene_type;
}

/**
 * Construct a list of genes of a particular gene type.
 */
char **new_genes(char alphabet, int n_genes, int n_states)
{
    int i, j, k, g = 0;
    char shifts[] = {LEFT, RIGHT};
    char **genes, gene[N_GENE_CHARS + 1] = {0};

    genes = calloc(n_genes, sizeof *genes);
    ptr_exit_on_err("calloc", genes);

    gene[READ] = alphabet;
    for (i = 0; i < n_alphabets; ++i) {
        gene[WRITE] = alphabets[i];
        for (j = 0; j < N_SHIFT_TYPES; ++j) {
            gene[SHIFT] = shifts[j];
            for (k = 0; k < n_states; ++k) {
                gene[NEXT_ST] = k + '0';
                genes[g++] = strdup(gene);
            }
        }
    }

    return genes;
}

void free_gene_types(GeneType **gene_types)
{
    int gt, g;

    for (gt = 0; gt < n_alphabets; ++gt) {
        for (g = 0; g < gene_types[gt]->n_genes; ++g)
            free(gene_types[gt]->genes[g]);
        free(gene_types[gt]->genes);
        free(gene_types[gt]);
    }

    free(gene_types);
}

void free_population(Chromosome **population)
{
    int p;

    for (p = 0; p < N_POPULATION; ++p) {
        if (population[p] != NULL) {
            free(population[p]->spec);
            free(population[p]);
        }
    }

    free(population);
}
