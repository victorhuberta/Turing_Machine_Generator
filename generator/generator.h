#ifndef GENERATOR_H
#define GENERATOR_H

#include "../main.h"
#include "../interpreter/interpreter.h"
#include "../population/population.h"
#include "../evolution/evolution.h"
#include "../fitness/fitness.h"

#define N_ALLOWED_GENERATIONS 400
#define N_FITTEST 999.0

void init_generator(char *_alphabets, char *_input, char *_output);
void generate_machine(void);
int test_solutions(Chromosome **population, double *total_fitness);
void _print_correct_machine(char *spec, long n_generations);
void _print_generate_failure(long n_generations);

#endif
